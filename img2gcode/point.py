from __future__ import annotations


class Point:
    EQUAL_THRESH = 0.001

    def __init__(self, x: float=0, y: float=0):
        if isinstance(x, tuple) or isinstance(x, list):
            self.x = float(x[0])
            self.y = float(x[1])
        else:
            self.x = float(x)
            self.y = float(y)

    def __len__(self) -> int:
        return 2

    def __getitem__(self, i: int) -> float:
        if not 0 <= i <= 1:
            raise IndexError(f"Index out of range: {i}")

        return self.x if i == 0 else self.y

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return f"(x: {self.x}, y: {self.y})"

    def __imul__(self, other: float) -> Point:
        self.x *= other
        self.y *= other
        return self

    def __mul__(self, other: float) -> Point:
        return Point(self.x * other, self.y * other)

    def __eq__(self, other) -> bool:
        dx = abs(other.x - self.x)
        dy = abs(other.y - self.y)

        return dx <= Point.EQUAL_THRESH and dy <= Point.EQUAL_THRESH
