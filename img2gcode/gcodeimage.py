from typing import List, Union, Tuple
from math import modf
from random import randint
from .edgefinder import EdgeFinder
from .enums import Origin
from .point import Point
from .stringwriter import StringWriter
from .properties import Properties


class GCodeImage(Properties):
    """
    Transforms images into gcode
    """

    def __init__(self):
        super().__init__([
            ("origin", Origin.CARTESIAN, Origin),
            ("firmware_retract", False, bool),
            ("firmware_homing", False, bool),
            ("layers", 1, int, lambda x: x >= 1),
            ("offset", 1, float, lambda x: x != 0),
            ("move_speed", 83.33, float, lambda x: x > 0),
            ("draw_speed", 83.33, float, lambda x: x > 0)
        ])

        self.edge = EdgeFinder()
        self._extent = None
        self._lines = None

    def _frmt(self, val: float) -> Union[str, Tuple[str]]:
        f = lambda x: f"{x:.2f}"

        if isinstance(val, list) or isinstance(val, tuple):
            return tuple(map(f, val))

        return f(val)

    def _point(self, p: Point) -> str:
        m = Point(p.x, p.y)
        m.y = self._extent.y - m.y

        if self.origin is Origin.CENTER:
            m.x -= self._extent.x / 2
            m.y -= self._extent.y / 2

        x, y = self._frmt((m.x, m.y))
        return f"X{x} Y{y}"

    def _retract(self, li: int) -> str:
        if self.firmware_retract:
            return "G10"

        z_off = self._frmt(20 + li * self.offset)
        return f"G0 Z{z_off}"

    def _unretract(self, li: int) -> str:
        if self.firmware_retract:
            return "G11"

        z_off = self._frmt(li * self.offset)
        return f"G0 Z{z_off}"

    def _gen_gcode(self, sw: StringWriter):
        sp = f"{self.move_speed * 60:.0f}"
        esp = f"{self.draw_speed * 60:.0f}"

        sw.write("G90 G21")     # Absolute, millimeters

        # Home axes
        if self.firmware_homing:
            sw.write(f"G0 F{sp}")
            sw.write("G28 X Y")
        else:
            sw.write(f"G0 X0 Y0 F{sp}")

        for li in range(self.layers):
            z_off = self._frmt(li * self.offset)
            sw.write(f"G0 Z{z_off}")

            for line in self._lines:
                length = len(line)

                # Randomize starting/ending position on closed loops
                # when drawing many layers
                if line.closed and li > 0:
                    sixth = length // 6
                    index = randint(sixth, max(0, min(length - sixth, length - 1)))
                    stop = 0
                else:
                    index = 0
                    stop = 1

                sw.write(self._retract(li))
                sw.write(f"G0 {self._point(line[index])}")
                sw.write(self._unretract(li))
                sw.write(f"G1 F{esp}")

                index += 1

                while stop < length:
                    sw.write(f"G1 {self._point(line[index])}")
                    index = (index + 1) % length
                    stop += 1

                sw.write(f"G0 F{sp}")

        sw.write(self._retract(self.layers - 1))

        # Home X and Y
        if self.firmware_homing:
            sw.write("G28 X Y")
        else:
            sw.write("G0 X0 Y0")

    def read(self, image: str):
        """Read image from path"""

        self.edge.read(image)

    def write(self, filename: str, extra: List[str]=None):
        """Write image to path"""

        to_stdout = filename == "-"

        if not to_stdout and not filename.lower().endswith(".gcode"):
            filename += ".gcode"

        self._extent, self._lines = self.edge.process()

        sw = StringWriter("\n" if to_stdout else "\r\n")

        if extra is not None:
            sw.writelines(map(lambda s: "; " + s, extra))

        self._gen_gcode(sw)

        if to_stdout:
            sw.seek(0)
            print(sw.read(), end="")
        else:
            sw.save(filename, mode="w", encoding="ascii")

        sw.close()

        self._lines = None
