import numpy
import matplotlib.pyplot as plt
from functools import partial as bind
from typing import List, Tuple, Union
from scipy.signal import savgol_filter
from skimage import transform, exposure, feature, morphology
from skimage.io import imread
from .line import Line, PointLike
from .point import Point
from .misc import search_space
from .math import clamp, average, median
from .properties import Properties


class EdgeFinderException(Exception):
    pass


class EdgeFinder(Properties):
    """Finds edges in an image"""

    def __init__(self):
        super().__init__([
            ("size", 512, int, lambda x: x > 0),
            ("smoothing", 1, float, lambda x: 0 <= x <= 5),
            ("gamma", 0, float, lambda x: x == 0 or 0.5 <= x <= 4),
            ("gap", 1, float, lambda x: x > 0),
            ("post_process", 1, float, lambda x: x > 0),
            ("skeleton", False, bool),
            ("threshold", 2, int, lambda x: x >= 1),
            ("scaling", 1, float, lambda x: x > 0),
            ("culling", 0, float, lambda x: x >= 0)
        ])

        self._savgol = {
            "window_length": 7,
            "polyorder": 4,
            "mode": "nearest"
        }

        self._image = None
        self._edges = None
        self._visited = None

    def _process_image(self, img: numpy.ndarray) -> numpy.ndarray:
        if img is None:
            raise EdgeFinderException("Image has not been read")

        height, width = img.shape
        larger = max(height, width)

        if larger != self.size:
            delta = self.size / larger
            size = (round(height * delta), round(width * delta))
            img = transform.resize(img, size, anti_aliasing=True)

        if self.skeleton:
            img = morphology.skeletonize(img)
            img = numpy.around(img, decimals=0)
        elif self.gamma != 0:
            img = exposure.adjust_gamma(img, self.gamma)

        return img

    def _near(self, p1: PointLike, p2: PointLike) -> bool:
        dist = self.threshold ** 2 * self.scaling * self.gap
        return Line.distance(p1, p2) <= dist

    def _filter(self, ln: Line) -> Line:
        length = len(ln)
        x_arr = numpy.zeros(length, dtype=float)
        y_arr = numpy.zeros(length, dtype=float)

        for i, p in enumerate(ln):
            x_arr[i] = p.x
            y_arr[i] = p.y

        x_arr = savgol_filter(x_arr, **self._savgol)
        y_arr = savgol_filter(y_arr, **self._savgol)

        # Reset first and last point
        x_arr[0],  y_arr[0]  = ln[0]
        x_arr[-1], y_arr[-1] = ln[-1]

        return Line(zip(y_arr, x_arr))

    def _post_process(self, ln: Line) -> Line:
        dist = self.threshold * self.scaling * self.post_process
        index = 1               # first and last lines
        points = len(ln) - 1    # are always added
        first, last = ln[0], ln[-1]
        processed = Line()
        processed.append(first)

        # Close line if end and start are close together
        if self._near(first, last):
            last = first

        while index < points:
            ap = ln[index]
            near = []
            index += 1

            if dist != 0:
                for j in range(index, points):
                    if Line.distance(ap, ln[j]) <= dist:
                        near.append(ln[j])
                    else:
                        break

                if len(near) > 0:
                    index += len(near)
                    ap = Point(average(near))

            # Distance between two points shouldn't be below 1 millimeter
            if Line.distance(processed[-1], ap) > 1:
                processed.append(ap)

        if Line.distance(processed[-1], last) > 1:
            processed.append(last)
        else:
            processed[-1] = last

        return self._filter(processed)

    def _find_edges(self, img: numpy.ndarray) -> numpy.ndarray:
        edges = None

        if self.skeleton:
            edges = img.astype(bool)
        else:
            if self.smoothing == 0:
                sigma = 1
            else:
                sigma = clamp(1.5 * self.smoothing, 1, 6)

            edges = feature.canny(img, sigma)

        return edges

    def _find_adjacent(self, row: int, col: int, search: int=1) -> List[PointLike]:
        adjacent = []
        current = (row, col)
        mr, mc = self._edges.shape

        for r, c in search_space(row, col, mr, mc, search):
            if r == row and c == col:
                continue

            if self._edges[r][c] and not self._visited[r][c]:
                adjacent.append((r, c))

        adjacent.sort(key=bind(Line.distance, current))

        return adjacent

    def _walk(self, row: int, col: int) -> Tuple[Line, int, int]:
        retr, retc = row, col
        adj = self._find_adjacent(row, col, self.threshold)
        self._visited[row][col] = True

        ln = Line()
        ln.append((row * self.scaling, col * self.scaling))

        # if the starting position has several adjacent edges,
        # split them into two blocks
        if len(adj) > 1:
            new_adj = []

            for rc in adj:
                dr, dc = rc[0] - row, rc[1] - col

                if dr <= 0 or dc >= 0:
                    new_adj.append(rc)

            adj = new_adj

        while len(adj) > 0:
            for r, c in adj:
                self._visited[r][c] = True

            row, col = adj[-1]
            retr, retc = row, col
            next_adj = self._find_adjacent(row, col, self.threshold)

            if len(next_adj) > 0:
                row, col = median(adj, int)

            ln.append((row * self.scaling, col * self.scaling))
            adj = next_adj

        if len(ln) < 2:
            return None, retr, retc
        else:
            return ln, retr, retc

    def _connect(self, ln1: Line, ln2: Line) -> Union[Line, bool]:
        ln = None
        ext = None

        if self._near(ln1[0], ln2[0]):
            ln = ln1.copy()
            ln.reverse()
            ext = ln2[1:]
        elif self._near(ln1[0], ln2[-1]):
            ln = ln2.copy()
            ext = ln1[1:]
        elif self._near(ln1[-1], ln2[0]):
            ln = ln1.copy()
            ext = ln2[1:]
        elif self._near(ln1[-1], ln2[-1]):
            ln = ln1.copy()
            ext = ln2[1:].copy()
            ext.reverse()

        if ln is not None:
            ln.extend(ext)

            if self._near(ln[0], ln[-1]):
                ln[-1] = ln[0]

            return ln

        return False

    def _finalize(self, lines: List[Line]) -> List[Line]:
        had_connection = len(lines) > 1

        # Connect lines if their starting/ending points are close together
        while had_connection:
            length = len(lines)
            had_connection = False
            connected = []
            skip = []

            for i in range(length):
                if i in skip:
                    continue

                line = lines[i]

                if line.closed:
                    connected.append(line)
                    continue

                found_match = False

                for j in range(i + 1, length):
                    if j in skip:
                        continue

                    conn = self._connect(line, lines[j])

                    if conn is False:
                        continue

                    found_match = True
                    had_connection = True
                    skip.append(j)
                    connected.append(conn)
                    break

                if not found_match:
                    connected.append(line)

            lines = connected

        cull = self.culling
        return [l for l in map(self._post_process, lines) if l.length > cull]

    def read(self, image: str):
        """Read image from path"""

        self._image = imread(image, as_gray=True)

    def process(self) -> Tuple[Point, List[Line]]:
        """
        Process image and return a sequence of lines

        Returns a tuple of:
            image extent (maximum x and y), lines
        """

        self._edges = self._find_edges(self._process_image(self._image))
        self._visited = numpy.full(self._edges.shape, False)

        rows, cols = self._edges.shape
        extent = Point(cols, rows) * self.scaling
        t = average((rows // 6, cols // 6), int)
        lines = []

        for srow in range(rows):
            for scol in range(cols):
                search = [(srow, scol)]
                proximity = False

                while len(search) > 0:
                    rr, cc = search.pop()

                    if not self._edges[rr][cc] or self._visited[rr][cc]:
                        self._visited[rr][cc] = True
                        continue

                    line, r, c = self._walk(rr, cc)

                    if line is None:
                        break

                    lines.append(line)

                    if not proximity:
                        search = [tpl for tpl in search_space(r, c, rows, cols, t)]
                        proximity = True

        final = self._finalize(lines)

        self._edges = None
        self._visited = None

        return extent, final
