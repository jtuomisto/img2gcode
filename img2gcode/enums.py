from enum import Enum


class Origin(Enum):
    CENTER = 1      # Origin at center   (coordinates both sides of center point)
    CARTESIAN = 2   # Origin at bottom left corner (coordinates always positive)
