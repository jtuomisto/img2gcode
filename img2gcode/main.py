import sys
from typing import List
from argparse import ArgumentParser
from itertools import chain
from img2gcode import GCodeImage, Origin


def handle_args(arguments: List[str]) -> object:
    epilog = (
        "The resulting gcode will have a size of: (--size value) * (--mmpp value)"
    )
    parser = ArgumentParser(prog="img2gcode",
                            description="Image to gcode transformer",
                            epilog=epilog)

    parser.add_argument(
        "-i", "--input",
        required=True,
        metavar="FILE",
        help="input image"
    )

    parser.add_argument(
        "-o", "--output",
        required=True,
        metavar="FILE",
        help="output file (- for stdout)"
    )

    parser.add_argument(
        "--params",
        action="store_true",
        help="write parameters used to the gcode"
    )

    parser.add_argument(
        "--cartesian",
        dest="origin",
        const=Origin.CARTESIAN,
        action="store_const",
        help="origin at bottom left"
    )

    parser.add_argument(
        "--center",
        dest="origin",
        const=Origin.CENTER,
        action="store_const",
        help="origin at center"
    )

    parser.add_argument(
        "--skeleton",
        action="store_true",
        help="skeletonize image (reduce connected areas to a single pixel wide skeleton)"
    )

    parser.add_argument(
        "--fw-retract",
        dest="firmware_retract",
        action="store_true",
        help="use firmware (un)retract (G10, G11)"
    )

    parser.add_argument(
        "--fw-homing",
        dest="firmware_homing",
        action="store_true",
        help="use firmware homing (G28)"
    )

    parser.add_argument(
        "-l", "--layers",
        default=1,
        type=int,
        metavar="INT",
        help="number of layers"
    )

    parser.add_argument(
        "-z", "--offset",
        default=1,
        type=float,
        metavar="FLOAT",
        help="layer offset in mm"
    )

    parser.add_argument(
        "-m", "--move-speed",
        dest="move_speed",
        default=83.33,
        type=float,
        metavar="FLOAT",
        help="speed (feedrate) in mm/s (during G0)"
    )

    parser.add_argument(
        "-d", "--draw-speed",
        dest="draw_speed",
        default=83.33,
        type=float,
        metavar="FLOAT",
        help="speed (feedrate) in mm/s (during G1)"
    )

    parser.add_argument(
        "-t", "--threshold",
        default=2,
        type=int,
        metavar="INTEGER",
        help="window size for line search"
    )

    parser.add_argument(
        "-c", "--scaling",
        default=1,
        type=float,
        metavar="FLOAT",
        help="pixels to millimeters conversion (in mm/pixel)"
    )

    parser.add_argument(
        "-u", "--cull",
        dest="culling",
        default=0,
        type=float,
        metavar="FLOAT",
        help="cull lines shorter than this (millimeters)"
    )

    parser.add_argument(
        "-s", "--size",
        default=512,
        type=int,
        metavar="INTEGER",
        help="longer image edge length in pixels"
    )

    parser.add_argument(
        "-S", "--smoothing",
        default=1,
        type=float,
        metavar="FLOAT",
        help="smoothing strength (< 1 less, > 1 more; 0 to disable)"
    )

    parser.add_argument(
        "-g", "--gamma",
        default=0,
        type=float,
        metavar="FLOAT",
        help="gamma adjustment (< 1 less, > 1 more; 0 to disable)"
    )

    parser.add_argument(
        "-G", "--gap",
        default=1,
        type=float,
        metavar="FLOAT",
        help="gap closing strength"
    )

    parser.add_argument(
        "-p", "--post-process",
        dest="post_process",
        default=1,
        type=float,
        metavar="FLOAT",
        help="post-process strength"
    )

    return parser.parse_args(arguments)


def main():
    args = handle_args(sys.argv[1:])
    args_dict = vars(args)
    extra = None

    gimg_props = ["origin", "move_speed", "draw_speed", "firmware_retract",
                  "firmware_homing", "layers", "offset"]
    edge_props = ["threshold", "scaling", "culling", "size", "smoothing",
                  "gamma", "gap", "post_process", "skeleton"]

    if args.params:
        extra = [f"{p}: {args_dict[p]!r}" for p in chain(gimg_props, edge_props)]

    try:
        gimg = GCodeImage()

        for obj, props in ((gimg, gimg_props), (gimg.edge, edge_props)):
            for p in props:
                setattr(obj, p, args_dict[p])

        gimg.read(args.input)
        gimg.write(args.output, extra)
    except ValueError as e:
        sys.stderr.write("ERROR - " + str(e) + "\n")
