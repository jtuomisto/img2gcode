import math
from typing import Sequence, Any, Type


def clamp(value: float, mini: float, maxi: float) -> float:
    return min(max(value, mini), maxi)


def median(seq: Sequence, return_type: Type=None) -> Any:
    length = len(seq)

    if length == 0:
        return 0

    if length % 2 == 0:
        middle = length // 2
        return average((seq[middle - 1], seq[middle]), return_type)

    item = seq[(length - 1) // 2]

    if return_type is None:
        return item

    if not hasattr(item, "__len__"):
        return return_type(item)

    return tuple([return_type(val) for val in item])


def average(seq: Sequence, return_type: Type=None) -> Any:
    length = len(seq)

    if length == 0:
        return 0

    if hasattr(seq[0], "__radd__"):
        avg = sum(seq) / len(seq)

        if math.modf(avg)[0] == 0:
            avg = int(avg)

        return avg if return_type is None else return_type(avg)

    if not hasattr(seq[0], "__len__"):
        raise TypeError("Sequence cannot be averaged")

    avgs = []

    for i in range(len(seq[0])):
        avgs.append(average([s[i] for s in seq], return_type))

    return tuple(avgs)
