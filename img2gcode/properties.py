from typing import Type, Callable, List, Tuple, Optional, Any


class _Property:
    __slots__ = ["value", "value_type", "validator"]

    def __init__(self, default: Any, value_type: Type, validator: Callable=None):
        self.value = value_type(default)
        self.value_type = value_type
        self.validator = validator


class Properties:
    def __init__(self, props: List[Tuple[str, Any, Type, Optional[Callable]]]=None):
        props_dict = {}

        if props is not None:
            props_dict = {p[0]: _Property(*p[1:]) for p in props}

        self.__dict__["_props_"] = props_dict

    def __getattr__(self, name: str) -> Any:
        if name in self._props_:
            return self._props_[name].value

        return object.__getattr__(self, name)

    def __setattr__(self, name: str, value: Any):
        if name not in self._props_:
            return object.__setattr__(self, name, value)

        prop = self._props_[name]
        val = prop.value_type(value)

        if prop.validator is None or prop.validator(val):
            prop.value = val
        else:
            raise ValueError(f"Invalid value for {name}: {value}")
