from __future__ import annotations
import math
from typing import Union, Tuple
from .point import Point


PointLike = Union[Tuple[int, int], Point]


def _point(p: PointLike) -> Point:
    if not isinstance(p, Point):
        if len(p) == 2:
            return Point(p[1], p[0])

        raise ValueError("Not a point")

    return p


class Line(list):
    """Container for a set of points"""

    @staticmethod
    def distance(*args) -> float:
        """
        Calculate distance traveled between points

        Points can be either Point instances or a (row, column) tuple
        """

        if len(args) < 2:
            return 0

        distance = 0

        for i in range(len(args) - 1):
            p1 = _point(args[i])
            p2 = _point(args[i + 1])
            distance += math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2)

        return distance

    def __init__(self, iterable=[], copy: bool=False):
        super().__init__([_point(p) for p in iterable])
        self._length = 0

        if len(self) > 1 and not copy:
            self._length = self._calculate_length()

    def __imul__(self, other: float) -> Line:
        for p in self:
            p *= other

        self._length *= other
        return self

    def __getitem__(self, key) -> Point:
        if isinstance(key, slice):
            return Line(super().__getitem__(key))

        return super().__getitem__(key)

    def __setitem__(self, key, value):
        index = self._valid_index(key)
        points = len(self)
        start = max(0, index - 1)
        stop = min(points, index + 2)
        old_length = self._calculate_length(start, stop)

        super().__setitem__(index, _point(value))

        if points > 1:
            new_length = self._calculate_length(start, stop)
            self._length += new_length - old_length

    def __delitem__(self, key):
        index = self._valid_index(key)
        super().__delitem__(index)
        self._length = self._calculate_length()

    def _valid_index(self, index: int) -> int:
        if index < 0:
            index += len(self)

        if index > len(self) - 1 or index < 0:
            raise IndexError(f"Index out of bounds: {index}")

        return index

    def _calculate_length(self, start: int=0, stop: int=None) -> float:
        points = len(self)

        if points < 2:
            return 0

        length = 0
        start = 0 if start is None or start < 0 else start
        stop = points - 1 if stop is None or stop >= points else stop

        for i in range(start, stop):
            length += Line.distance(self[i], self[i + 1])

        return length

    def copy(self) -> Line:
        new = Line(self, copy=True)
        new._length = self._length
        return new

    def insert(self, index, p: PointLike):
        index = self._valid_index(index)
        start = max(0, index)
        stop = min(len(self), index + 2)
        old_length = self._calculate_length(start, stop)

        super().insert(index, _point(p))
        points = len(self)

        if points > 1:
            start = max(0, index - 1)
            stop = min(points, index + 2)
            new_length = self._calculate_length(start, stop)
            self._length += new_length - old_length

    def append(self, p: PointLike):
        p = _point(p)

        if len(self) > 0:
            self._length += Line.distance(self[-1], p)

        super().append(p)

    def extend(self, lp: Union[Line, List[PointLike]]):
        if len(lp) == 0:
            return

        if isinstance(lp, Line):
            gap = 0

            if len(self) > 0:
                gap = Line.distance(self[-1], lp[0])

            super().extend(lp)
            self._length += gap + lp.length
        else:
            current = max(0, len(self) - 1)
            super().extend([_point(p) for p in lp])

            if len(self) > 1:
                self._length += self._calculate_length(current)

    def prepend(self, p: PointLike):
        p = _point(p)

        if len(self) > 0:
            self._length += Line.distance(p, self[0])

        super().insert(0, p)

    def delete(self, indexes: Union[int, List[int]]):
        if isinstance(indexes, int):
            indexes = [indexes]

        if len(indexes) > 0:
            d = 0

            for i in indexes:
                super().__delitem__(i - d)
                d += 1

            self._length = self._calculate_length()

    @property
    def length(self) -> float:
        return self._length

    @property
    def closed(self) -> bool:
        if len(self) < 3:
            return False

        return self[0] == self[-1]
