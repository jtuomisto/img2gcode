from typing import Tuple


def search_space(row: int, col: int, max_row: int, max_col: int, size: int=1) -> Tuple[int, int]:
    sr = range(max(0, row - size), min(max_row, row + size + 1))
    sc = range(max(0, col - size), min(max_col, col + size + 1))

    for r in sr:
        for c in sc:
            yield r, c
