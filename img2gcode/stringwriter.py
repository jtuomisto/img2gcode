from typing import List
from io import StringIO, SEEK_SET, SEEK_END
from shutil import copyfileobj


class StringWriter(StringIO):
    """Wrapper for StringIO"""

    def __init__(self, eol: str="\n"):
        super().__init__(newline="")
        self._eol = eol

    def __del__(self):
        if not self.closed:
            self.close()

    def write(self, line: str) -> int:
        written = super().write(line)
        written += super().write(self._eol)
        return written

    def writelines(self, lines: List[str]) -> int:
        written = 0

        for line in lines:
            written += self.write(line)

        return written

    def save(self, filename: str, mode: str="w", encoding="utf8"):
        current = self.tell()
        self.seek(0, SEEK_END)

        if self.tell() == 0:
            raise IOError("Buffer is empty")

        self.seek(0, SEEK_SET)

        with open(filename, mode=mode, encoding=encoding, newline="") as out:
            copyfileobj(self, out)

        self.seek(current, SEEK_SET)
