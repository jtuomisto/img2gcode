from setuptools import setup, find_packages

setup(
    name="img2gcode",
    version="0.1",
    description="Image to gcode transformer",
    keywords="gcode image",
    author="Jyri Tuomisto",
    author_email="jyri.tuomisto@pm.me",
    license="Unlicense",
    python_requires=">=3.7",
    packages=find_packages(exclude=["dist", "img2gcode.egg-info", "test_data", "test_result"]),
    install_requires=["numpy", "scipy", "scikit-image", "matplotlib"],
    exclude_package_data={'': ['README.md']},
    entry_points={"console_scripts": ["img2gcode=img2gcode.main:main"]},
    url="https://gitlab.com/jtuomisto/img2gcode"
)
