import matplotlib.pyplot as plt
from os import path
from img2gcode import EdgeFinder, GCodeImage, Origin


IMAGES = ["test_data/kekkonen.jpg",
          "test_data/kekkonen2.jpg",
          "test_data/musk.jpg",
          "test_data/pallo.png",
          "test_data/pallo2.png"]


def test():
    fig = 1

    for img in IMAGES:
        finder = EdgeFinder()
        finder.read(img)
        plt.figure(fig)
        plt.imshow(finder._find_edges(), cmap=plt.cm.gray)
        plt.axis("off")
        fig += 1

    plt.show()


def test2():
    img = IMAGES[1]
    finder = EdgeFinder()
    finder.read(img)
    lines = finder.process(4, 0.5)
    print(len(lines))
    print(repr(lines))


def test3():
    for img in IMAGES:
        out = path.join("test_result", "center_" + path.basename(img).partition(".")[0])
        gimg = GCodeImage(Origin.CENTER, 4, 0.5, 2)
        gimg.read(img)
        gimg.write(out)


def test4():
    for img in IMAGES:
        out = path.join("test_result", "cart_" + path.basename(img).partition(".")[0])
        gimg = GCodeImage(Origin.CARTESIAN, 4, 0.5, 2)
        gimg.read(img)
        gimg.write(out)


def test5():
    fig = 1

    for img in IMAGES[3:5]:
        finder = EdgeFinder(512, 0.4, 1)
        finder.read(img)
        plt.figure(fig)
        plt.imshow(finder._find_edges(), cmap=plt.cm.gray)
        plt.axis("off")
        fig += 1

    plt.show()


if __name__ == "__main__":
    #test()
    #test2()
    #test3()
    test4()
    #test5()
