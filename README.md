# img2gcode

Transforms images into gcode

### Dependencies

- numpy
- scipy
- scikit-image
- matplotlib

### Install

Install with `pip`
